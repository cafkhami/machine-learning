function p = predictGeneric(ThetaIn, ThetaHid, ThetaOut, num_hid, X)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

% Useful values
m = size(X, 1);
num_labels = size(ThetaOut, 1);

% You need to return the following variables correctly 
p = zeros(size(X, 1), 1);

aHid =zeros(m,size(ThetaIn,1)+1,num_hid);

a1 = [ones(m, 1) X];
aHid(:,:,1) = [ones(m,1) sigmoid(a1 * ThetaIn')];
if num_hid >1
    for i = 2:num_hid
        aHid(:,:,i) = [ones(m,1) sigmoid(aHid(:,:,i-1)*Thetahid(:,:,i-1)')]; 
    end
end
aOut = sigmoid(aHid(:,:,num_hid) * ThetaOut');
[dummy, p] = max(aOut, [], 2);

% =========================================================================


end
