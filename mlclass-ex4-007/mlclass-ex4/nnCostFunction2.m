function [J grad] = nnCostFunction2(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   hidden_layer_num, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
input_layer_end = hidden_layer_size * (input_layer_size + 1);
inner_layer_end = input_layer_end + hidden_layer_size * (hidden_layer_size + 1) * (hidden_layer_num - 1);

ThetaIn = reshape(nn_params(1:input_layer_end), hidden_layer_size, (input_layer_size + 1));

ThetaHid = reshape( ...
    nn_params(input_layer_end + 1:inner_layer_end), ...
    hidden_layer_size, ...
    (hidden_layer_size + 1), ...
    hidden_layer_num - 1 ...
);

ThetaOut = reshape(nn_params(inner_layer_end + 1:end),  num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
ThetaIn_grad = zeros(size(ThetaIn));
ThetaHid_grad = zeros(size(ThetaHid));
ThetaOut_grad = zeros(size(ThetaOut));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%
% Add bias units to training inputs
orig_X = X;
X = [ones(m, 1) X];

% Convert training labels to vectors
orig_y = y;
y = eye(num_labels);
y = y(orig_y, :);

% Do forward-propagation
aIn = [ones(m, 1) sigmoid(X * ThetaIn')];

aHid = zeros(size(aIn));
aPrev = aIn;
for i = 1:(hidden_layer_num - 1)
    ThetaI = ThetaHid(:, :, i);

    aHid(:, :, i) = [ones(m, 1) sigmoid(aPrev * ThetaI')];
    aPrev = aHid(:, :, i);
end

aOut = sigmoid(aPrev * ThetaOut');

% Calculate J using a vectorized approach
A = -y .* log(aOut) - (1 - y) .* log(1 - aOut);
reg = ( ...
    lambda / (2 * m) * ...
    ( ...
        sum(sum(ThetaIn(:, 2:end).^2)) + ...
        sum(sum(sum(ThetaHid(:, 2:end, :).^ 2))) + ...
        sum(sum(ThetaOut(:, 2:end) .^ 2)) ...
    ) ...
);
J = sum(A(:)) / m + reg;

deltaOut = aOut - y;

deltaHid = zeros(size(deltaOut));
deltaPrev = deltaOut;
ThetaPrev = ThetaOut;
for i = fliplr(1:(hidden_layer_num - 1))
    aI = aHid(:, :, i);

    deltaHid(:, :, i) = (deltaPrev * ThetaPrev) .* (aIn .* (1 - aIn));
    deltaPrev = deltaHid(:, :, i);
    ThetaPrev = ThetaHid(:, :, i);
end

deltaIn = (deltaPrev * ThetaPrev) .* (aIn .* (1 - aIn));

if hidden_layer_num > 1
    aPrev = aHid(:, :, end);
else
    aPrev = aIn;
end

ThetaOut_grad = ( ...
    ( ...
        deltaOut' * aPrev + ...
        lambda * [zeros(size(ThetaOut,1), 1) ThetaOut(:, 2:end)] ...
    ) ./ ...
    m ...
);

for i = fliplr(1:(hidden_layer_num - 1))
    deltaI = deltaHid(:, :, i);
    ThetaI = ThetaHid(:, :, i);

    if i > 1
        aPrev = aHid(:, :, i - 1);
    else
        aPrev = aIn;
    end

    ThetaHid_grad(:, :, i) = ( ...
        ( ...
            deltaI' * aPrev + ...
            lambda * [zeros(size(ThetaI,1), 1) ThetaI(:, 2:end)] ...
        ) ./ ...
        m ...
    );
end

ThetaIn_grad = ( ...
    ( ...
        deltaIn(:, 2:end)' * X + ...
        lambda * [zeros(size(ThetaIn,1), 1) ThetaIn(:, 2:end)] ...
    ) ./ ...
    m ...
);
% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [ThetaIn_grad(:); ThetaHid_grad(:); ThetaOut_grad(:)];

end
