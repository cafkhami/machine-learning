function idx = findClosestCentroids(X, centroids)
%FINDCLOSESTCENTROIDS computes the centroid memberships for every example
%   idx = FINDCLOSESTCENTROIDS (X, centroids) returns the closest centroids
%   in idx for a dataset X where each row is a single example. idx = m x 1 
%   vector of centroid assignments (i.e. each entry in range [1..K])
%

% Set K
[K n] = size(centroids);
m = size(X, 1);

% You need to return the following variables correctly.
idx = zeros(size(X,1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Go over every example, find its closest centroid, and store
%               the index inside idx at the appropriate location.
%               Concretely, idx(i) should contain the index of the centroid
%               closest to example i. Hence, it should be a value in the 
%               range 1..K
%
% Note: You can use a for-loop over the examples to compute this.
%
% Make m-by-n-by-K matrix from centroids
centroids = reshape(centroids', [1 n K]);
centroids = repmat(centroids, [m 1 1]);

% Make m-by-n-by-K matrix from samples
X = repmat(X, [1 1 K]);

% Compute differences
differences = X - centroids;

% Find squared normal of all differences
squares = sum(differences .* differences, 2);

% Find indices of minimum squares
[_min idx] = min(squares, [], 3);
% =============================================================
end

